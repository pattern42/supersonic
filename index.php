<?php

 /*** error reporting on ***/
 error_reporting(E_ALL);

 /*** define the site path ***/
 $site_path = realpath(dirname(__FILE__));
 $url_path = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
 
 define ('__SITE_PATH', $site_path);
 define ('__URL_PATH', $url_path);

 /*** include the init.php file ***/
 include 'includes/init.php';
 
 /*** load the router ***/
 $registry->router = new router($registry);
 
 /*** set the controller path ***/
 $registry->router->setPath (__SITE_PATH . '/controller');

 /*** load up the template ***/
 $registry->template = new template($registry);

 /*** load the controller ***/
 $registry->router->loader();
 
 
 
/* End of file index.php */
/* Location: ./index.php */

